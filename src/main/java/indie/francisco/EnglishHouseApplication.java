package indie.francisco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EnglishHouseApplication {
    public static void main(String[] args) {
        SpringApplication.run(EnglishHouseApplication.class, args);
    }
}
