package indie.francisco.englishhouse.web.adapter;

public interface Adapter<Request, Response, Entity> {

    Entity mapToEntity(Request request);

    Response mapToWeb(Entity entity);
}
