package indie.francisco.englishhouse.model.enums;

public enum DocumentType {
    CPF, CNPJ
}
