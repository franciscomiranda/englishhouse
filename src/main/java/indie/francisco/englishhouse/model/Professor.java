package indie.francisco.englishhouse.model;

import indie.francisco.englishhouse.model.enums.DocumentType;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;

@Entity
@Table(name = "professor")
public class Professor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "professor_id")
    private Long professorId;

    @Column(name = "professor_first_name")
    private String professorFirstName;

    @Column(name = "professor_last_name")
    private String professorLastName;

    @Column(name = "professor_birth")
    private LocalDate professorBirthDate;

    @NotEmpty(message = "Document number must not be empty.")
    @Column(name = "professor_document", unique = true)
    private String professorDocumentNumber;

    @Enumerated(EnumType.STRING)
    @Column(name = "professor_document_type")
    private DocumentType professorDocumentType;

    @Column(name = "professor_phone")
    private String professorPhone;

    @Column(name = "professor_email")
    private String professorEmail;

    @Column(name = "professor_active")
    private Boolean professorIsActive;

    @OneToOne(targetEntity = Address.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "professor_address_id", referencedColumnName = "address_id")
    private Address professorAddress;

    public Long getProfessorId() {
        return professorId;
    }

    public void setProfessorId(Long professorId) {
        this.professorId = professorId;
    }

    public String getProfessorFirstName() {
        return professorFirstName;
    }

    public void setProfessorFirstName(String professorFirstName) {
        this.professorFirstName = professorFirstName;
    }

    public String getProfessorLastName() {
        return professorLastName;
    }

    public void setProfessorLastName(String professorLastName) {
        this.professorLastName = professorLastName;
    }

    public LocalDate getProfessorBirthDate() {
        return professorBirthDate;
    }

    public void setProfessorBirthDate(LocalDate professorBirthDate) {
        this.professorBirthDate = professorBirthDate;
    }

    public String getProfessorDocumentNumber() {
        return professorDocumentNumber;
    }

    public void setProfessorDocumentNumber(String professorDocumentNumber) {
        this.professorDocumentNumber = professorDocumentNumber;
    }

    public DocumentType getProfessorDocumentType() {
        return professorDocumentType;
    }

    public void setProfessorDocumentType(DocumentType professorDocumentType) {
        this.professorDocumentType = professorDocumentType;
    }

    public String getProfessorPhone() {
        return professorPhone;
    }

    public void setProfessorPhone(String professorPhone) {
        this.professorPhone = professorPhone;
    }

    public String getProfessorEmail() {
        return professorEmail;
    }

    public void setProfessorEmail(String professorEmail) {
        this.professorEmail = professorEmail;
    }

    public Boolean getProfessorIsActive() {
        return professorIsActive;
    }

    public void setProfessorIsActive(Boolean professorIsActive) {
        this.professorIsActive = professorIsActive;
    }

    public Address getProfessorAddress() {
        return professorAddress;
    }

    public void setProfessorAddress(Address professorAddress) {
        this.professorAddress = professorAddress;
    }
}
