package indie.francisco.englishhouse.model;

import javax.persistence.*;

@Entity
@Table(name = "material_category")
public class MaterialCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "material_category_id")
    private Long materialCategoryId;

    @Column(name = "material_category_name")
    private String materialCategoryName;

    @Lob
    @Column(name = "material_category_description")
    private String materialCategoryDescription;

    @Column(name = "material_category_active")
    private Boolean materialCategoryIsActive;

    public Long getMaterialCategoryId() {
        return materialCategoryId;
    }

    public void setMaterialCategoryId(Long materialCategoryId) {
        this.materialCategoryId = materialCategoryId;
    }

    public String getMaterialCategoryName() {
        return materialCategoryName;
    }

    public void setMaterialCategoryName(String materialCategoryName) {
        this.materialCategoryName = materialCategoryName;
    }

    public String getMaterialCategoryDescription() {
        return materialCategoryDescription;
    }

    public void setMaterialCategoryDescription(String materialCategoryDescription) {
        this.materialCategoryDescription = materialCategoryDescription;
    }

    public Boolean getMaterialCategoryIsActive() {
        return materialCategoryIsActive;
    }

    public void setMaterialCategoryIsActive(Boolean materialCategoryIsActive) {
        this.materialCategoryIsActive = materialCategoryIsActive;
    }
}
