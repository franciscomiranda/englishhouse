package indie.francisco.englishhouse.model;

import indie.francisco.englishhouse.model.enums.DocumentType;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;

@Entity
@Table(name = "student")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "student_id")
    private Long studentId;

    @NotEmpty(message = "First name must not be empty.")
    @Column(name = "student_first_name")
    private String studentFirstName;

    @Column(name = "student_last_name")
    private String studentLastName;

    @Column(name = "student_birth_date")
    private LocalDate studentBirthDate;

    @NotEmpty(message = "Document must not be empty.")
    @Column(name = "student_document_number", unique = true)
    private String studentDocumentNumber;

    @Enumerated(EnumType.STRING)
    @Column(name = "student_document_type")
    private DocumentType studentDocumentType;

    @Column(name = "student_phone")
    private String studentPhone;

    @Column(name = "student_email")
    private String studentEmail;

    @Column(name = "student_active")
    private boolean studentIsActive;

    @OneToOne(targetEntity = Address.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "student_address_id", referencedColumnName = "address_id")
    private Address studentAddress;

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public String getStudentFirstName() {
        return studentFirstName;
    }

    public void setStudentFirstName(String studentFirstName) {
        this.studentFirstName = studentFirstName;
    }

    public String getStudentLastName() {
        return studentLastName;
    }

    public void setStudentLastName(String studentLastName) {
        this.studentLastName = studentLastName;
    }

    public LocalDate getStudentBirthDate() {
        return studentBirthDate;
    }

    public void setStudentBirthDate(LocalDate studentBirthDate) {
        this.studentBirthDate = studentBirthDate;
    }

    public String getStudentDocumentNumber() {
        return studentDocumentNumber;
    }

    public void setStudentDocumentNumber(String studentDocumentNumber) {
        this.studentDocumentNumber = studentDocumentNumber;
    }

    public DocumentType getStudentDocumentType() {
        return studentDocumentType;
    }

    public void setStudentDocumentType(DocumentType studentDocumentType) {
        this.studentDocumentType = studentDocumentType;
    }

    public String getStudentPhone() {
        return studentPhone;
    }

    public void setStudentPhone(String studentPhone) {
        this.studentPhone = studentPhone;
    }

    public String getStudentEmail() {
        return studentEmail;
    }

    public void setStudentEmail(String studentEmail) {
        this.studentEmail = studentEmail;
    }

    public boolean isStudentIsActive() {
        return studentIsActive;
    }

    public void setStudentIsActive(boolean studentIsActive) {
        this.studentIsActive = studentIsActive;
    }

    public Address getStudentAddress() {
        return studentAddress;
    }

    public void setStudentAddress(Address studentAddress) {
        this.studentAddress = studentAddress;
    }
}
