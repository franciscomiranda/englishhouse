package indie.francisco.englishhouse.model;

import java.util.HashSet;
import java.util.Set;

public class Material {

    private Long materialId;
    private String materialName;
    private MaterialCategory materialCategory;
    private Set<Course> materialCourses = new HashSet<>();

}
