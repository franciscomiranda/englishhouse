package indie.francisco.englishhouse.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "class")
public class Class {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "class_id")
    private Long classId;

    @Column(name = "class_reference_name")
    private String classReferenceName;

    @ManyToOne(targetEntity = Course.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "class_course_id", referencedColumnName = "course_id")
    private Course classCourse;

    @Column(name = "class_student")
    @ElementCollection(targetClass = Student.class)
    private Set<Student> classStudents = new HashSet<>();

    @ManyToOne(targetEntity = Professor.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "class_professor_id", referencedColumnName = "professor_id")
    private Professor classProfessor;

    @Column(name = "class_start_date")
    private Timestamp classStartDate;

    @Column(name = "class_end_date")
    private LocalDate classEndDate;

    public Long getClassId() {
        return classId;
    }

    public void setClassId(Long classId) {
        this.classId = classId;
    }

    public String getClassReferenceName() {
        return classReferenceName;
    }

    public void setClassReferenceName(String classReferenceName) {
        this.classReferenceName = classReferenceName;
    }

    public Course getClassCourse() {
        return classCourse;
    }

    public void setClassCourse(Course classCourse) {
        this.classCourse = classCourse;
    }

    public Set<Student> getClassStudents() {
        return classStudents;
    }

    public void setClassStudents(Set<Student> classStudents) {
        this.classStudents = classStudents;
    }

    public Professor getClassProfessor() {
        return classProfessor;
    }

    public void setClassProfessor(Professor classProfessor) {
        this.classProfessor = classProfessor;
    }

    public Timestamp getClassStartDate() {
        return classStartDate;
    }

    public void setClassStartDate(Timestamp classStartDate) {
        this.classStartDate = classStartDate;
    }

    public LocalDate getClassEndDate() {
        return classEndDate;
    }

    public void setClassEndDate(LocalDate classEndDate) {
        this.classEndDate = classEndDate;
    }
}
