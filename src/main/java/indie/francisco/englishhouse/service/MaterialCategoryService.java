package indie.francisco.englishhouse.service;

import indie.francisco.englishhouse.model.MaterialCategory;
import indie.francisco.englishhouse.repository.MaterialCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MaterialCategoryService {

    @Autowired
    MaterialCategoryRepository materialCategoryRepository;

    public MaterialCategory create(MaterialCategory materialCategory){
        materialCategory.setMaterialCategoryIsActive(true);
        return materialCategoryRepository.save(materialCategory);
    }

    public List<MaterialCategory> findAll(){
        return materialCategoryRepository.findAll();
    }

    public Optional<MaterialCategory> findById(Long materialCategoryId){
        return materialCategoryRepository.findById(materialCategoryId);
    }

    public List<MaterialCategory> findByActiveTrue(){
        return materialCategoryRepository.findByMaterialCategoryIsActiveTrue();
    }

    public MaterialCategory update(MaterialCategory materialCategory){
        return materialCategoryRepository.save(materialCategory);
    }

    public MaterialCategory updateActiveStatus(MaterialCategory materialCategory){
        materialCategory.setMaterialCategoryIsActive(!materialCategory.getMaterialCategoryIsActive());
        return materialCategoryRepository.save(materialCategory);
    }
}
