package indie.francisco.englishhouse.service;

import java.util.List;

public interface Service {

    Object create(Object object);
    Object readById(Long id);
    List<Object>readAll();
    Object update(Object object);
    void delete(Long id);
}
