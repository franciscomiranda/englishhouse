package indie.francisco.englishhouse.repository;

import indie.francisco.englishhouse.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Long> {
}
