package indie.francisco.englishhouse.repository;

import indie.francisco.englishhouse.model.MaterialCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MaterialCategoryRepository extends JpaRepository<MaterialCategory, Long> {
    List<MaterialCategory> findByMaterialCategoryIsActiveTrue();
}
