package indie.francisco.englishhouse.controller;

import indie.francisco.englishhouse.model.Professor;
import indie.francisco.englishhouse.repository.ProfessorRepository;
import indie.francisco.englishhouse.service.ProfessorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.xml.ws.Response;

@RestController
@RequestMapping("professor/")
public class ProfessorController {

    @Autowired
    private ProfessorService professorService;

    @Autowired
    private ProfessorRepository professorRepository;

    @PostMapping("create")
    public ResponseEntity<?> createProfessor(@Valid @RequestBody Professor professor) {
        professor.setProfessorIsActive(true);
        return ResponseEntity.status(HttpStatus.CREATED).body(professorRepository.save(professor));
    }

    @GetMapping("readall")
    public ResponseEntity<?> readAllProfessor() {
        return ResponseEntity.status(HttpStatus.OK).body(professorRepository.findAll());
    }

    @GetMapping("readbyid/{id}")
    public ResponseEntity<?> readProfessorById(@PathVariable("id") Long professorId){
        return ResponseEntity.status(HttpStatus.OK).body(professorRepository.findById(professorId));
    }

    @PutMapping("update")
    public ResponseEntity<?> updateProfessor(@Valid @RequestBody Professor professor){
        return ResponseEntity.status(HttpStatus.OK).body(professorRepository.save(professor));
    }

    @PutMapping("updateactivestatus")
    public ResponseEntity<?> updateProfessorActiveStatus(@Valid @RequestBody Professor professor){
        professor.setProfessorIsActive(professor.getProfessorIsActive() ? false : true);
        return ResponseEntity.status(HttpStatus.OK).body(professorRepository.save(professor));
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<Void> deleteProfessor(@PathVariable("id") Long professorId) {
        try {
            professorRepository.deleteById(professorId);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (Exception exception) {
            return ResponseEntity.badRequest().build();
        }
    }
}
