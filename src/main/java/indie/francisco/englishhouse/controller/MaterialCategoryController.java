package indie.francisco.englishhouse.controller;

import indie.francisco.englishhouse.model.MaterialCategory;
import indie.francisco.englishhouse.repository.MaterialCategoryRepository;
import indie.francisco.englishhouse.service.MaterialCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("materialcategory/")
public class MaterialCategoryController {

    @Autowired
    private MaterialCategoryService materialCategoryService;

    @Autowired
    private MaterialCategoryRepository materialCategoryRepository;

    @PostMapping("create")
    public ResponseEntity<MaterialCategory> createMaterialCategory(@Valid @RequestBody MaterialCategory materialCategory) {
        return ResponseEntity.status(HttpStatus.CREATED).body(materialCategoryService.create(materialCategory));
    }

    @GetMapping("readall")
    public ResponseEntity<List<MaterialCategory>> readAllMaterialCategory() {
        return ResponseEntity.status(HttpStatus.OK).body(materialCategoryService.findAll());
    }

    @GetMapping("readbyid/{id}")
    public ResponseEntity<Optional<MaterialCategory>> readMaterialCategoryById(@PathVariable("id") Long materialCategoryId){
        return ResponseEntity.status(HttpStatus.OK).body(materialCategoryService.findById(materialCategoryId));
    }

    @GetMapping("readactives")
    public ResponseEntity<List<MaterialCategory>> readActiveMaterialCategory(){
        return ResponseEntity.status(HttpStatus.OK).body(materialCategoryService.findByActiveTrue());
    }

    @PutMapping("update")
    public ResponseEntity<MaterialCategory> updateMaterialCategory(@Valid @RequestBody MaterialCategory materialCategory) {
        return ResponseEntity.status(HttpStatus.OK).body(materialCategoryService.update(materialCategory));
    }

    @PutMapping("updateactivestatus")
    public ResponseEntity<MaterialCategory> updateMaterialCategoryActiveStatus(@Valid @RequestBody MaterialCategory materialCategory) {
        return ResponseEntity.status(HttpStatus.OK).body(materialCategoryService.updateActiveStatus(materialCategory));
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<Void> deleteMaterialCategory(@PathVariable("id") Long materialCategoryId){
        return ResponseEntity.status(HttpStatus.OK).build(); // TODO when MATERIAL is built
    }
}
