package indie.francisco.englishhouse.controller;

import indie.francisco.englishhouse.model.Address;
import indie.francisco.englishhouse.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("address/")
public class AddressController {

    @Autowired
    private AddressRepository addressRepository;

    @PostMapping("create")
    public ResponseEntity<?> create(@Valid @RequestBody Address address) {
        return ResponseEntity.status(HttpStatus.OK).body(addressRepository.save(address));
    }

    @GetMapping("readall")
    public ResponseEntity<List<Address>> readAll() {
        return ResponseEntity.status(HttpStatus.OK).body(addressRepository.findAll());
    }

    @GetMapping("readone/{id}")
    public ResponseEntity<?> readById(@PathVariable Long addressId) {
        return ResponseEntity.status(HttpStatus.OK).body(addressRepository.findById(addressId));
    }

    @PutMapping("update")
    public ResponseEntity<?> update(@RequestBody Address address) {
        return ResponseEntity.status(HttpStatus.OK).body(addressRepository.save(address));
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long addressId) {
        try {
            addressRepository.deleteById(addressId);
            return ResponseEntity.status(HttpStatus.OK).build();
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }
}
