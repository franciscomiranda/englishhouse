package indie.francisco.englishhouse.controller;

import indie.francisco.englishhouse.model.Student;
import indie.francisco.englishhouse.repository.StudentRepository;
import indie.francisco.englishhouse.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("student/")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @Autowired
    private StudentRepository studentRepository;

    @PostMapping("create")
    public ResponseEntity<Student> createStudent(@Valid @RequestBody Student student) {
        student.setStudentIsActive(true);
        return ResponseEntity.status(HttpStatus.CREATED).body(studentRepository.save(student));
    }

    @GetMapping("readall")
    public ResponseEntity<List<Student>> readAllStudent() {
        return ResponseEntity.status(HttpStatus.OK).body(studentRepository.findAll());
    }

    @GetMapping("readbyid/{id}")
    public ResponseEntity<?> readStudentById(@PathVariable("id") Long studentId) {
        return ResponseEntity.status(HttpStatus.OK).body(studentRepository.findById(studentId));
    }

    @PutMapping("update")
    public ResponseEntity<?> updateStudent(@Valid @RequestBody Student student) {
        return ResponseEntity.status(HttpStatus.OK).body(studentRepository.save(student));
    }

    @PutMapping("updateactivestatus")
    public ResponseEntity<?> updateStudentActiveStatus(@RequestBody Student student){
        student.setStudentIsActive(student.isStudentIsActive() ? false : true);
        return ResponseEntity.status(HttpStatus.OK).body(studentRepository.save(student));
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long studentId) {
        try{
            studentRepository.deleteById(studentId);
            return ResponseEntity.status(HttpStatus.OK).build();
        }catch (Exception e){
            return ResponseEntity.badRequest().build();
        }
    }
}
